from flask import Blueprint, session, render_template, request, redirect, url_for
from flask_login import login_user, login_required, logout_user
from tinydb import where

from . import db
from .models import User

bp = Blueprint('auth', __name__, url_prefix='/auth')

# Routes blueprint
@bp.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        # Récupérer les identifiants du formulaire
        mail = request.form['signin_mail']
        password = request.form['signin_password']

        # Vérifier que l'utilisateur existe
        users = db.table('users').search(where('mail') == mail)
        if users != [] and users[0]['password'] == password:
            session['username'] = users[0]['firstname']
            session['roles'] = users[0]['roles']
            login_user(User(mail))
            return redirect(url_for('index'))
    return render_template('auth/login.html')


@bp.route('/logout')
@login_required
def logout():
    session.pop('username', None)
    logout_user()
    return redirect(url_for('index'))
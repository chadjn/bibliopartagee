'use strict';

var bookLists = document.querySelectorAll('.book-list');

if (bookLists) {
    for (var bookList of bookLists) {
        if (bookList.children.length == 2) {
            bookList.classList.add('only-two-books');
        }
    }
}

function loadTable(e) {
    var tables = document.querySelectorAll('table');
    var tableDisplayed = document.querySelector('table.display');
    var tableToDisplay = e.innerHTML;

    for (var table of tables) {
        table.classList.remove("display");
    }

    if (Number.isInteger(parseInt(tableToDisplay))) {
        tableToDisplay = parseInt(tableToDisplay) - 1;
    } else {
        if (e.ariaLabel == 'Précédent') {
            if(tableDisplayed.id.split("-")[1] == 0) {
                tableToDisplay = 0;
            } else {
                tableToDisplay = parseInt(tableDisplayed.id.split("-")[1]) - 1;
            }
        } else {
            if (tableDisplayed.id.split("-")[1] == tables.length - 1) {
                tableToDisplay = tables.length - 1;
            } else {
                tableToDisplay = parseInt(tableDisplayed.id.split("-")[1]) + 1;
            }
        }
    }

    tables[tableToDisplay].classList.add("display");
}

function requestLoan() {
    var loanForm = document.querySelector('.lending-book');
    loanForm.classList.toggle('d-none');
}

function switchMessage() {
    var loanMessage = document.querySelector('#message');
    var loanStep = document.querySelector('#step');

    switch (loanStep.value) {
        case "2":
            loanMessage.value = "Réservé";
            break;
        case "3":
            loanMessage.value = "Emprunté";
            break;
        case "4":
            loanMessage.value = "Prêt confirmé";
            break;
        case "5":
            loanMessage.value = "Retour"
            break;
        case "6":
            loanMessage.value = "Retour confirmé"
            break;
        default:
            loanMessage.value = "Refusé";
    }
}

function sendMessage() {
    var loanForm = document.querySelector('.lending-message');
    loanForm.classList.toggle('d-none');
    switchMessage();
}

function reject() {
    var loanStep = document.querySelector('#step');
    loanStep.value = 0;
    switchMessage();
}

function addBook() {
    var addForm = document.querySelector('.new-book');
    addForm.classList.toggle('d-none');
}

function deleteBook(specimen = undefined) {
    var deleteForm = document.querySelector('.delete-book');
    deleteForm.classList.toggle('d-none');

    if (specimen) {
        var specimenRefs = document.querySelectorAll('.specimen-reference');
        var bookTitle = document.querySelector('.specimen-book-title');
        var bookAuthor = document.querySelector('.specimen-book-author');
        var inputValue = document.querySelector('#specimen_ref')

        for (var specimenRef of specimenRefs) {
            specimenRef.innerHTML = specimen.ref;
        }
        bookTitle.innerHTML = specimen.book.title;
        bookAuthor.innerHTML = specimen.book.author;
        inputValue.value = specimen.ref;
    }
}
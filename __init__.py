# -*- coding: utf-8 -*-

# Imports
from flask import Flask, session, render_template, request, redirect, url_for
from flask_login import LoginManager, login_required
from tinydb import TinyDB, where
import datetime
from .models import User

#Démarrer l'application flask
app = Flask(__name__)
app.secret_key = b'_5#y2L"F4Q8zVu]'

# Liaison avec la base de données
db = TinyDB('LendingLibrary/db.json')

# Création du login manager
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'auth.login'

# Variable définissant la taille des tableaux d'affichage
size = 10

# Recherche utilisateur
@login_manager.user_loader
def load_user(mail):
    # Le mail est l'identificateur de l'utilisateur (un mail = un utilisateur)
    if db.table('users').search(where('mail') == mail) != []:
        return User(id)
    else:
        return None

# Blueprint auth
from . import auth
app.register_blueprint(auth.bp)

# Page d'accueil après connexion
@app.route('/', methods=['GET'])
@login_required
def index():
    # Récupération de tous les livres présents en base de données
    books = db.table('books').all()
    # Récupération de tous les genres présents en base de données
    genres = db.table('genres').all()

    # Rendu de la page dynamique avec les livres et les genres récupérés
    # Les nouveautés correspondent aux quatre derniers livres présents en base de données
    return render_template('core/index.html', newBooks=books[-4:], books=books, genres=genres)

# Page de recherche
@app.route('/recherche', methods=['GET'])
@login_required
def searching_page():
    # Récupération de tous les genres présents en base de données
    genres = db.table('genres').all()

    # Rendu de la page dynamique avec les genres récupérés
    return render_template('core/search.html', genres=genres)

# Page de résultats de la recherche
@app.route('/resultat', methods=['GET', 'POST'])
@login_required
def searching_results():
    # Si on accède à la page avec la méthode POST (une recherche a été effectuée)
    if request.method == 'POST':
        # Récupération des données du formulaires
        title = request.form['book_title']
        author = request.form['book_author']
        isbn = request.form['book_isbn']
        genres = request.form.getlist('book_genres')
    # Sinon si on accède à la page avec la méthode GET (on veut accéder à tous les exemplaires d'un même livre)
    elif request.method == 'GET':
        # Récupération de l'identificateur de livre et instanciation des autres variables
        title = None
        author = None
        isbn = request.args['book']
        genres = []

    # Récupération de tous les livres présents en base données correspondant à au moins l'un de champ de recherche du formulaire (hors genres)
    searchedResults = db.table('books').search((where('title') == title) | (where('author') == author) | (where('isbn') == isbn))

    # Ajout des livres correspondant aux genres sélectionnés
    for book in db.table('books').all():
        for bookGenre in book['genres']:
            for genre in genres:
                if bookGenre == int(genre):
                    searchedResults.append(book)

    # Suppression des doublons
    for book in searchedResults:
        if searchedResults.count(book) > 1:
            searchedResults.remove(book)

    searchedBooks = []

    # Ajout des informations nécessaires à identifier le livre (exemplaire et prêteur)
    for book in searchedResults:
        bookSpecimens = db.table('specimens').search(where('book') == book['isbn'])

        # Si des exemplaires existent, chercher leurs propriétaires
        if bookSpecimens:
            for specimen in bookSpecimens:
                specimenLender = db.table('users').search(where('mail') == specimen['lender'])

                # Si le prêteur existe, l'ajouter aux données de l'exemplaire
                if specimenLender:
                    specimen['lenderName'] = specimenLender[0]['firstname'] + " " + specimenLender[0]['lastname']

                # Ajouter alors les données du livre aux données de l'exemplaire
                specimen['book'] = book

                # Ajouter l'exemplaire aux résultats à renvoyer
                searchedBooks.append(specimen)

    # S'il existe au moins un livre correspondant à la recherche, renvoyer les résultats
    if searchedBooks != []:
        # Variable récupérant le nombre de livres à afficher
        nb_books = len(searchedBooks)
        return render_template('core/search_result.html', books=searchedBooks, nb_books=nb_books, size=size)

    if request.method == 'POST':
        # Par défaut, renvoie à la page de recherche
        return redirect(url_for('searching_page'))
    elif request.method == 'GET':
        # Par défaut, renvoie à la page d'index
        return redirect(url_for('index'))

# Page d'aperçu de tous les emprunts d'un utilisateur (restreint au rôle borrower)
@app.route('/emprunts', methods=['GET'])
def view_emprunts():
    # Récupération de tous les livres que l'utilisateur a emprunté
    loans = db.table('loans').search(where('borrower') == session['_user_id'])

    # Ajout des informations nécessaires à identifier l'emprunt (exemplaire, livre et prêteur)
    for loan in loans:
        specimen = db.table('specimens').search(where('ref') == loan['specimen'])

        # Si l'exemplaire existe, chercher le livre et le prêteur associé
        if specimen:
            book = db.table('books').search(where('isbn') == specimen[0]['book'])
            lender = db.table('users').search(where('mail') == specimen[0]['lender'])

            # Si le livre existe, l'ajouter aux données de l'exemplaire
            if book:
                specimen[0]['book'] = book[0]

            # Si le prêteur existe, l'ajouter aux données de l'exemplaire
            if lender:
                specimen[0]['lenderName'] = lender[0]['firstname'] + " " + lender[0]['lastname']

            # Ajouter les données de l'exemplaire aux données de l'emprunt
            loan['specimen'] = specimen[0]

    # Variable récupérant le nombre d'emprunts à afficher
    nb_loans = len(loans)

    # Rendu de la page dynamique avec les emprunts récupérés
    return render_template('core/lending.html', loans=loans, nb_loans=nb_loans, size=size)

# Page d'aperçu de la bibliothèque d'un utilisateur (restreint au rôle lender)
@app.route('/bibliotheque', methods=['GET', 'POST'])
def view_bibliotheque():
    # Si on accède à la page avec la méthode POST (un livre est ajouté ou supprimé)
    if request.method == 'POST':
        if len(request.form) != 1 :
            book_title = request.form['book_title']
            book_author = request.form['book_author']
            book_isbn = request.form['book_isbn']
            book_publiedYear = request.form['book_publiedYear']
            book_edition = request.form['book_edition']
            book_cover = request.form['book_cover']
            book_coverDescription = request.form['book_coverDescription']
            book_genres = request.form.getlist('book_genres')

            # Récupération de tous les livres correspondant à l'ISBN
            books = db.table('books').search(where('isbn') == book_isbn)
            # Si le livre n'existe pas déjà en base de données, l'ajouter
            if not(books):

                # Changer les genres de chaîne de caractères à des entiers
                genres = []
                for book_genre in book_genres:
                    genres.append(int(book_genre))

                # Ajouter le nouveau livre dans la base de données avec les données récupérées correspondantes
                db.table('books').insert({"title": book_title,
                                          "author": book_author,
                                          "isbn": book_isbn,
                                          "publiedYear": int(book_publiedYear),
                                          "edition": book_edition,
                                          "cover": book_cover,
                                          "coverDescription": book_coverDescription,
                                          "genres": genres})

                # Définition du début de la référence du livre (nom de l'auteur + année de publication) selon les données récupérées
                ref = book_author.split(' ')[1].lower() + str(book_publiedYear)

                # Définition du numéro d'incrémentation au format XXX (initialisation à 001 puisque le livre vient d'être créé donc il ne peut exister d'exemplaire)
                id_number = "001"
            else:
                # Définition du début de la référence du livre (nom de l'auteur + année de publication) selon les données présentes en base de données
                ref = books[0]['author'].split(' ')[1].lower() + str(books[0]['publiedYear'])

                # Récupération de tous les exemplaires correspondant au livre et à l'utilisateur
                specimensBook = db.table('specimens').search((where('book') == book_isbn) & (where('lender') == session['_user_id']))

                # Définition du numéro d'incrémentation au format XXX (ajout de 0 devant en conséquence)
                if len(specimensBook) < 9:
                    id_number = "00" + str(len(specimensBook) + 1)
                elif 9 < len(specimensBook) < 99:
                    id_number = "0" + str(len(specimensBook) + 1)
                else:
                    id_number = str(len(specimensBook) + 1)

            # Récupération des données utilisateur correspondant à l'utilisateur
            users = db.table('users').search(where('mail') == session['_user_id'])

            # Ajout du numéro en base de données du prêteur à la référence
            ref += str(users[0].doc_id) if users else "0"

            # Ajout du numéro d'incrémentation au format XXX à la référence
            ref += id_number

            # Ajouter le nouvel exemplaire dans la base de données avec :
            # une référence sous la forme : nom de l'auteur + année de publication + numéro en base de données du prêteur + numéro d'incrémentation au format XXX ;
            # l'ISBN récupéré comme identifiant de livre ;
            # l'identifiant de l'utilisateur (session['_user_id']) comme identifiant de prêteur ;
            # l'état initialisé à disponible.
            db.table('specimens').insert({"ref": ref,
                                          "book": book_isbn,
                                          "lender": session['_user_id'],
                                          "state": 1})

        else:
            specimen_ref = request.form['specimen_ref']
            db.table('specimens').remove(where("ref") == specimen_ref)

    # Récupération de tous les exemplaires que l'utilisateur a prêté
    specimens = db.table('specimens').search(where('lender') == session['_user_id'])

    # Ajout des informations nécessaires à identifier l'exemplaire (livre)
    for specimen in specimens:
        book = db.table('books').search(where('isbn') == specimen['book'])

        # Si le livre existe, l'ajouter aux données de l'exemplaire
        if book:
            specimen['book'] = book[0]

    # Variable récupérant le nombre d'exemplaires à afficher
    nb_specimens = len(specimens)

    # Récupération de tous les genres présents en base de données
    genres = db.table('genres').all()

    # Rendu de la page dynamique avec les exemplaires récupérés
    return render_template('core/library.html', genres=genres, specimens=specimens, nb_specimens=nb_specimens, size=size)

# Page d'aperçu d'un livre
@app.route('/livre/<string:reference>', methods=['GET', 'POST'])
@login_required
def view_livre(reference: str):
    # Si on accède à la page avec la méthode POST (un message a été envoyé (demande d'emprunt comprise))
    if request.method == 'POST':
        send = datetime.date.today().strftime("%d/%m/%Y")
        loan_date = None
        loan_time = None
        loan_message = None
        loan_id = None
        loan_step = None

        # Récupération des données du formulaires
        for rform in request.form:
            loan_date = request.form['loan_date'] if rform == "loan_date" else loan_date
            loan_time = request.form['loan_time'] if rform == "loan_time" else loan_time
            loan_message = request.form['loan_message'] if rform == "loan_message" else loan_message
            loan_id = request.form['loan_id'] if rform == "loan_id" else loan_id
            loan_step = request.form['loan_step'] if rform == "loan_step" else loan_step

        # Si le formulaire envoyé est celui de demande d'emprunt, définir les dates de début et de fin d'emprunt
        if loan_date and loan_time:
            start = datetime.datetime.strptime(loan_date, '%Y-%m-%d')
            end = start + datetime.timedelta(int(loan_time))

            # Ajouter le nouvel emprunt dans la base de données avec :
            # une incrémentation de l'id ;
            # la date du jour comme date de demande d'emprunt (requesting) ;
            # la date de réservation initialisée vide ;
            # les dates d'emprunt et de retour (loaning pour le début et returning pour la fin) ;
            # les états d'emprunt et de retour initialisés à 0 (aucun utilisateur n'a renseigné l'exemplaire comme emprunté ou retourné)
            # l'identifiant de l'utilisateur (session['_user_id']) comme emprunteur ;
            # l'identifiant de l'exemplaire (reference).
            db.table('loans').insert({"id": len(db.table('loans').all()) + 1,
                                      "requesting": send,
                                      "booking": "",
                                      "loaning": start.date().strftime('%d/%m/%Y'),
                                      "loaned": 0,
                                      "returning": end.date().strftime('%d/%m/%Y'),
                                      "returned": 0,
                                      "borrower": session['_user_id'],
                                      "specimen": reference})

            # Ajouter le nouveau message dans la base de données avec :
            # une incrémentation de l'id ;
            # le dernier emprunt créé pour identifiant de l'emprunt (d'où l'importance de créer le prêt avant de créer le message);
            # l'identifiant de l'utilisateur (session['_user_id']) comme auteur du message ;
            # un message prédéfini avec les dates de l'emprunt ;
            # la date du jour comme date d'envoi du message ;
            # l'état du message initialisé à non-lu.
            db.table('messages').insert({"id": len(db.table('messages').all()) + 1,
                                         "loan": len(db.table('loans').all()),
                                         "author": session['_user_id'],
                                         "content": "Emprunt du " + start.date().strftime('%d/%m') + " au " + end.date().strftime('%d/%m') + " ?",
                                         "send": send,
                                         "state": 0})

            # Considérer que l'exemplaire est indisponible dès qu'une demande de prêt dessus est faite
            db.table('specimens').update({"state": 0}, where("ref") == reference)

        # Sinon, l'étape d'emprunt (loan_step) définira le message à envoyer et l'action à effectuer
        elif loan_message and loan_id:
            # À l'étape 2, la date de réservation est mise à jour
            if loan_step == "2":
                db.table('loans').update({"booking": send}, where("id") == int(loan_id))

            # À l'étape 3, la date d'emprunt est mise à jour et l'emprunt est considéré comme effectué pour le prêteur
            elif loan_step == "3":
                db.table('loans').update({"loaning": send, "loaned": 1}, where("id") == int(loan_id))

            # À l'étape 4, l'emprunt est également considéré comme effectué par l'emprunteur
            elif loan_step == "4":
                db.table('loans').update({"loaned": 2}, where("id") == int(loan_id))

            # À l'étape 5, la date de retour est mise à jour et l'emprunt est considéré comme retourné pour l'emprunteur
            elif loan_step == "5":
                db.table('loans').update({"returning": send, "returned": 1}, where("id") == int(loan_id))

            # À l'étape 4, l'emprunt est également considéré comme retourné par le prêteur + l'exemplaire est considéré comme disponible
            elif loan_step == "6":
                db.table('loans').update({"returned": 2}, where("id") == int(loan_id))
                db.table('specimens').update({"state": 1}, where("ref") == reference)

            # Par défaut, renseigner le refus en remettant l'exemplaire disponible
            else:
                db.table('specimens').update({"state": 1}, where("ref") == reference)

            # Ajouter le nouveau message dans la base de données avec :
            # une incrémentation de l'id ;
            # l'identifiant de l'emprunt récupéré (loan_id);
            # l'identifiant de l'utilisateur (session['_user_id']) comme auteur du message ;
            # le message récupéré (loan_message) ;
            # la date du jour comme date d'envoi du message ;
            # l'état du message initialisé à non-lu.
            db.table('messages').insert({"id": len(db.table('messages').all()) + 1,
                                         "loan": int(loan_id),
                                         "author": session['_user_id'],
                                         "content": loan_message,
                                         "send": send,
                                         "state": 0})

    # Récupération de l'exemplaire correspondant à la référence
    specimens = db.table('specimens').search(where('ref') == reference)

    # Si l'exemplaire existe, chercher des informations complémentaires
    if specimens:
        specimen = specimens[0]

        # Récupération de tous les emprunts correspondant à l'exemplaire
        loansAsBorrower = db.table('loans').search((where('specimen') == specimen['ref']) & (where('borrower') == session['_user_id']))

        # Récupération de tous les prêts correspondant à l'exemplaire
        loansAsLender = []
        if specimen['lender'] == session['_user_id']:
            loansAsLender = db.table('loans').search((where('specimen') == specimen['ref']))

        # Défintion de s'il s'agit d'un prêt ou d'un emprunt
        loans = loansAsBorrower if loansAsBorrower else loansAsLender
        messages = []

        # S'il existe des emprunts ou des prêts
        if loans:
            # Récupération de tous les messages correspondant à chaque emprunt/prêt
            for loan in loans:
                loanMessages = db.table('messages').search(where('loan') == loan['id'])

                # S'il existe des messages liés à l'emprunt
                if loanMessages:
                    # Ajouter les messages à la liste des messages liés à l'exemplaire
                    messages += loanMessages

        # S'il existe des messages, les parcourir
        if messages:
            for message in messages:

                # Si l'auteur du message n'est pas l'utilisateur, rechercher l'auteur du message
                if message['author'] != session['_user_id']:
                    messageAuthor = db.table('users').search(where('mail') == message['author'])

                    # Si l'auteur du message existe, ajouter son prénom et son nom aux données du message
                    if messageAuthor:
                        message['authorName'] = messageAuthor[0]['firstname'] + " " + messageAuthor[0]['lastname']

                    # Changer le statut de ces messages en lu
                    db.table('messages').update({"state": 1}, where("id") == message['id'])

        lastLoan = []

        if loans:
            lastLoan = loans[-1]

        # Si les données du livre sont déjà passées dans l'exemplaire
        if isinstance(specimen['book'], tuple):
            # Rendu de la page dynamique avec le livre récupéré et le signifié comme non-emprunté (arbitrairement)
            return render_template('core/book.html', specimen=specimen, loan=lastLoan, messages=messages)
        
        # Récupération du prêteur correspondant à l'exemplaire
        specimenLender = db.table('users').search(where('mail') == specimen['lender'])

        # Récupération du livre correspondant à l'exemplaire
        books = db.table('books').search(where('isbn') == specimen['book'])

        # Si le prêteur existe, ajouter son prénom et son nom aux données de l'exemplaire
        if specimenLender:
            specimen['lenderName'] = specimenLender[0]['firstname'] + " " + specimenLender[0]['lastname']

        # Si le livre existe, l'ajouter aux données de l'exemplaire
        if books:
            specimen['book'] = books[0]

    # Rendu de la page dynamique avec le livre récupéré et le signifié comme non-emprunté (arbitrairement)
    return render_template('core/book.html', specimen=specimen, loan=lastLoan, messages=messages)

# Page d'aperçu de la messagerie d'un utilisateur
@app.route('/messagerie', methods=['GET'])
def view_messagerie():
    # Récupération des exemplaires dont l'utilisateur est le prêteur
    specimens = db.table('specimens').search(where('lender') == session['_user_id'])
    loansAsLender = []

    # Si les exemplaires existent, chercher pour chacun si un prêt existe
    if specimens:
        for specimen in specimens:
            loansToAdd = db.table('loans').search(where('specimen') == specimen['ref'])

            # Si un ou plusieurs prêt(s) existe(nt), cherche le(s) livre(s) et le(s) emprunteur(s) associé(s)
            if loansToAdd:
                books = db.table('books').search(where('isbn') == specimen['book'])
                users = db.table('users').search(where('mail') == loansToAdd[0]['borrower'])

                # Si le livre existe, l'ajouter aux données de l'exemplaire
                if books:
                    specimen['book'] = books[0]

                loansToAdd[0]['specimen'] = specimen

                # Si l'emprunteur existe, l'ajouter aux données de l'exemplaire
                if users:
                    loansToAdd[0]['borrowerName'] = users[0]['firstname'] + " " + users[0]['lastname']

                # Ajouter le prêt aux tableaux de prêts
                loansAsLender.append(loansToAdd[0])

    # Récupération des emprunts dont l'utilisateur est l'emprunteur
    loansAsBorrower = db.table('loans').search(where('borrower') == session['_user_id'])

    # Si les emprunts existent, chercher les exemplaires associés
    if loansAsBorrower:
        specimens = db.table('specimens').all()

        # Ne continuer à chercher que s'il existe des exemplaires en base de données
        if specimens:
            for loan in loansAsBorrower:
                for specimen in specimens:
                    # S'il existe un emprunt de l'exemplaire, chercher le livre et le prêteur associés
                    if loan['specimen'] == specimen['ref']:
                        books = db.table('books').search(where('isbn') == specimen['book'])
                        users = db.table('users').search(where('mail') == specimen['lender'])

                        # Si le livre existe, l'ajouter aux données de l'exemplaire
                        if books:
                            specimen['book'] = books[0]

                        # Si l'emprunteur existe, l'ajouter aux données de l'exemplaire
                        if users:
                            specimen['lenderName'] = users[0]['firstname'] + " " + users[0]['lastname']

                        # Ajouter les données de l'exemplaire aux données de l'emprunt
                        loan['specimen'] = specimen

    # Associer les prêts et les emprunts d'un même utilisateur
    loans = loansAsLender + loansAsBorrower

    userMessages = []

    # S'il existe des prêts et/ou emprunts, chercher les messages associés
    if loans:
        messages = db.table('messages').all()

        # Ne continuer à chercher que s'il existe des messages en base de données
        if messages:
            for loan in loans:
                for message in messages:
                    # S'il existe un message à propos du prêt/emprunt et que l'auteur de ce message n'est pas l'utilisateur, ajouter les données du prêt/emprunt aux données du message
                    if message['loan'] == loan['id'] and message['author'] != session['_user_id']:
                        message['loan'] = loan
                        # Ajouter le message à la liste des messages de l'utilisateur
                        userMessages.append(message)

    # Variable récupérant le nombre de messages à afficher
    nb_messages = len(userMessages)

    # Rendu de la page dynamique avec les messages récupérés
    return render_template('core/messages.html', messages=userMessages, nb_messages=nb_messages, size=size)


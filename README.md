# Share livre

Share Livre propose une application de partage de livres. Vous pouvez vous connecter en tant que prêteur et/ou emprunteur et naviguer à travers les nombreux exemplaires proposés par notre plateforme.

## Accueil
À partir de cette page, vous pouvez découvrir les derniers livres proposés par nos contributeurs sous la catégorie "Nouveauté". Vous pouvez également découvrir tous les livres sous différentes catégories.

## Recherche
Cette page vous permet de rechercher le ou les livres que vous souhaiteriez pouvoir emprunter. Attention, il est nécessaire d'écrire exactement au moins une information (en entier, avec la bonne casse et la bonne orthographe). De plus, les informations de recherche s'additionnent et ne se complètent pas (par exemple, si vous chercher "La Vague" comme titre et "Victor HUGO" comme auteur vous obtiendrez tous les livres ayant pour titre "La Vague" **et** tous les livres écrits par Victor HUGO, et non pas tous les livres écrits par Victor HUGO et ayant pour titre "La Vague").

## Mes emprunts
Si vous êtes un emprunteur, vous retrouverez ici tous les emprunts que vous avez effectués.

## Ma bibliothèque
Si vous êtes un prêteur, vous retrouverez ici tous les livres que vous proposez au prêt.

## Mes messages
Vous pourrez avoir un aperçu de tous vos messages sur cette page (peu importe le livre concerné ou s'il s'agit d'un message rattaché à un prêt ou un emprunt).

## Page de livre
Sur ces pages, vous avez un aperçu du livre emprunté/empruntable ou prêté/prêtable. Vous pouvez accéder aux actions disponibles pour chaque exemplaire et aux messages qui y sont liés. Les messages sont prédéfinis.
